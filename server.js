require('dotenv').config()

const session = require('client-sessions'),
      bodyParser = require('body-parser'),
      express = require('express'),
      helmet = require('helmet'),
      morgan = require('morgan'),
      cors = require('cors'),
      path = require('path'),
      fs = require('fs'),
      { MODE, PORT, SECRET_KEY } = process.env,
      app = express()

// middlewares
app.use(cors())
app.use(helmet())
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(session({
  cookieName: 'session',
  secret: SECRET_KEY,
  duration: 60 * 60 * 1000,
  activeDuration: 5 * 60 * 1000
}))

if (MODE === 'production') {
  app.use(express.static(path.join(__dirname, 'client', 'build')))
} else {
  app.use(express.static(path.join(__dirname, 'client')))
}

// Router's
app.use('/', require('./routes/users-r'))
app.use('/api', require('./routes/board-r'))
app.use('/', require('./routes/main-r'))

app.listen(PORT, () => {
  console.log(`App running port - ${PORT}, mode - ${MODE}`)
})

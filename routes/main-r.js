const router = require('express').Router(),
      fs = require('fs'),
      path = require('path')

router.get('*', (req, res) => {
  let html = fs.createReadStream(path.join(__dirname, '..', 'client', 'build', 'index.html'))

  res.writeHeader(200, {"Content-Type": "text/html"});  
  html.pipe(res)
})

module.exports = router

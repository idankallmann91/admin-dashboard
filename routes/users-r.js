const router = require('express').Router(),
      {
        findUserByUsername,
        comparePassword,
        cryptPassword,
        createUser,
        deleteUser
      } = require('../config/db.js')

router.post('/user-login', (req, res) => {
  let { username, password } = req.body
  console.log({ username, password })
  if(!username || !password) return res.json({success: false, mssg: 'Required\s all fields'})

  findUserByUsername(username).then(user => {
    if(!user) return res.json({success: false, mssg: 'Invalid username or password'})

    comparePassword(password, user.password).then(verifyPass => {
      if(!verifyPass) return res.json({success: false, mssg: 'Invalid username or password'})

      req.session.id = user._id
      req.session.username = user.username
      console.log(req.session);
      res.json({success: true, mssg: `Hi ${user.username}!!`})
    }).catch(e => console.log(e))

  }).catch(ex => console.log(ex))
})

router.post('/user-signup', (req, res) => {
  let { password, username } = req.body

  if(!password || !username) return res.json({success: false, mssg: 'Required\'s fields'})

  findUserByUsername(username).then(user => {
    if(user) return res.json({success: false, mssg: 'Username already exist'})

    cryptPassword(password).then(hash => {
      let regUser = { password: hash, username, loggedIn: false }

      createUser(regUser).then(newUser => {

        res.json({success: true, mssg: `Thank you for sign up ${username}`})
      }).catch(e => console.log(e))

    }).catch(ex => console.log(ex))

  }).catch(exp => console.log(exp))
})

router.post('/me', (req, res) => {
  console.log("api me");
  let loggedIn = req.session.id ? true : false
  console.log(req.session.id);
  if(!loggedIn) return res.json({success: false, data: false, mssg: 'Not logged in'})

  res.json({success: true, data: true, mssg: 'Logged in'})
})

router.get('/user-logout', (req, res) => {
  let { btn } = req.query,
      btnOnOff = btn === 'false' ? true : false

  res.json({success: btnOnOff})
})

module.exports = router

import React, { useState } from 'react'

import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import InboxIcon from '@material-ui/icons/MoveToInbox'
import MailIcon from '@material-ui/icons/Mail'
import './Header.css'

const Header = () => {
  const [drawer, setDrawer] = useState(false)

  return (
    <div>
        <AppBar position="static" className="header">
            <Toolbar>
                <IconButton onClick={() => setDrawer(true)} color="inherit" aria-label="Menu">
                    <MenuIcon />
                </IconButton>
                <Typography variant="h6" className="h6" color="inherit">
                  Admin dashboard
                </Typography>
            </Toolbar>
        </AppBar>
        <Drawer open={drawer} onClose={() => setDrawer(false)}>
            <div
              tabIndex={0}
              role="button"
              onClick={() => setDrawer(false)}
              onKeyDown={() => setDrawer(false)}
            >
                <div id="navDrawer">
                    <List>
                        {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                          <ListItem button key={text}>
                              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                              <ListItemText primary={text} />
                          </ListItem>
                        ))}
                    </List>
                    <Divider />
                    <List>
                        {['All mail', 'Trash', 'Spam'].map((text, index) => (
                          <ListItem button key={text}>
                              <ListItemIcon>{index % 2 === 0 ? <InboxIcon /> : <MailIcon />}</ListItemIcon>
                              <ListItemText primary={text} />
                          </ListItem>
                        ))}
                    </List>
                </div>
            </div>
        </Drawer>
    </div>
  )
}

export default Header

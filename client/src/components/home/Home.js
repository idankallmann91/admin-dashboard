import React, { useEffect } from 'react'
import './Home.css'

// Redux Connection
import { connect } from 'react-redux'

// redux actions
import { IsLogged } from '../../store/actions/user-a'
import { UploadFiles, ToggleButton, SaveText, SetText, SetCheckbox } from '../../store/actions/board-a'
import { AddImageFiles, RemoveImageFile, AddAudioFile, RemoveAudioFile } from '../../store/actions/files-a'

// Child components
import Keyboard from './keyboard/Keyboard'
import Notify from './notify/Notify'
import Categories from './categories/Categories'

const Home = (props) => {
  const {
     keyboard,
     setCheckbox,
     checkbox,
     toggleButton,
     isLogged,
     loggedIn, 
     history } = props

  useEffect(() => {
      isLogged().then(res => {
        if(res.user.success) return history.push('/login')
      }).catch(console.log)
    }, [])

  return (
    <div className="container">
        { loggedIn ? <div>Loading..</div> :
          <div>
              <h1>Admin Dashboard</h1>
              <Keyboard toggleBtn={keyboard} handleToggleButton={toggleButton} />

              <Notify setCheckbox={setCheckbox} checkbox={checkbox}/>

              <Categories {...props} />
          </div>
        }
    </div>
  )
}

const mapStateToProps = state => {
  return {
    loggedIn: state.userReducer.loggedIn,

    keyboard: state.boardReducer.keyboard,
    checkbox: state.boardReducer.checkbox,
    text: state.boardReducer.inputText,

    imgFiles: state.filesReducer.imageFiles,
    imgNames: state.filesReducer.imagesNames,
    audioF: state.filesReducer.audioFile
  }
}

const mapDispathToProps = dispatch => {
  return {
    isLogged: () => dispatch(IsLogged()),

    toggleButton: val => dispatch(ToggleButton(val)),

    setCheckbox: val => dispatch(SetCheckbox(val)),

    setText: val => dispatch(SetText(val)),
    saveText: val => dispatch(SaveText(val)),

    uploadFiles: files => dispatch(UploadFiles(files)),
    addImageFiles: files => dispatch(AddImageFiles(files)),
    removeImageFile: file => dispatch(RemoveImageFile(file)),
    addAudioFile: file => dispatch(AddAudioFile(file)),
    removeAudioFile: file => dispatch(RemoveAudioFile(file))

  }
}

export default connect(mapStateToProps, mapDispathToProps)(Home)

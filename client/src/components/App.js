import React from 'react'
import { BrowserRouter as Router, Route, Switch } from "react-router-dom"
import './App.css'

import Header from './header/Header'
import Login from './login/Login'
import Home from './home/Home'
// import Dashboard from './components/dashboard/Dashboard'
import Footer from './footer/Footer'

const App = () => {
  return (
    <Router>
        <div>
            <Header />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/login" component={Login} />
                <Route component={Home} />
            </Switch>
            <Footer />
        </div>
    </Router>
  )
};

export default App

import React, { useState } from 'react'
import './Signup.css'

// material-ui styles
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import IconButton from '@material-ui/core/IconButton'
import InputAdornment from '@material-ui/core/InputAdornment'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'

const SignUp = (props) => {
  const { classes, signup } = props,
        [username, setUsername] = useState(''),
        [password, setPassword] = useState(''),
        [showPassword, setShowPassword] = useState(false),
        [dialog, setDialog] = useState(false)

  return (
    <div id="signupBtn">
        <Button variant="outlined" color="primary" onClick={() => setDialog(true)}>
          Need an account?
        </Button>
        <Dialog
          open={dialog}
          onClose={() => setDialog(false)}
          aria-labelledby="form-dialog-title"
        >
            <form className={classes.container} noValidate autoComplete="off">
                <DialogTitle id="form-dialog-title">Signup</DialogTitle>
                <DialogContent>
                    <TextField
                      autoFocus
                      id="outlined-name"
                      label="Name"
                      className={classes.textField}
                      value={username}
                      onChange={e => setUsername(e.target.value)}
                      margin="normal"
                      variant="outlined"
                    />
                    <TextField
                      id="outlined-adornment-password"
                      label="Password"
                      type={showPassword ? 'text' : 'password'}
                      className={classes.textField}
                      value={password}
                      onChange={e => setPassword(e.target.value)}
                      margin="normal"
                      variant="outlined"
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="Toggle password visibility"
                              onClick={() => setShowPassword(!showPassword)}
                            >
                              {!showPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                          </InputAdornment>
                        ),
                      }}
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={() => setDialog(false)} color="primary">
                      Cancel
                    </Button>
                    <Button onClick={() => signup({username, password})} color="primary">
                      Signup
                    </Button>
                </DialogActions>
            </form>
        </Dialog>
    </div>
  )
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  }
})

export default withStyles(styles)(SignUp)

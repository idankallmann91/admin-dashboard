import React, { useState, useEffect } from 'react'
import './Login.css'

// Redux Connection
import { connect } from 'react-redux'

// redux actions
import { IsLogged, LoginUser, SignUp } from '../../store/actions/user-a'

// child components
import SignupModal from './signup/Signup'

// material-ui styles
import { withStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import IconButton from '@material-ui/core/IconButton'
import InputAdornment from '@material-ui/core/InputAdornment'
import Fab from '@material-ui/core/Fab'
import NavigationIcon from '@material-ui/icons/Navigation'

const Login = (props) => {
  const { isLogged, loggedIn, history, classes, loginUser, signUp } = props,
        [username, setUsername] = useState(''),
        [password, setPassword] = useState(''),
        [showPassword, setShowPassword] = useState(false)

  useEffect(() => {
      isLogged().then(res => {
        console.log(res);
        if(res.user.success) return history.push('/')
      }).catch(console.log)
    }, [loggedIn])

  return (
    <div>
        <div id="login">
            <h3>Login</h3>
            <form className={classes.container} noValidate autoComplete="off">
                <div id="loginForm">
                    <TextField
                      autoFocus
                      id="outlined-name"
                      label="Name"
                      className={classes.textField}
                      value={username}
                      onChange={e => setUsername(e.target.value)}
                      margin="normal"
                      variant="outlined"
                    />
                    <TextField
                      id="outlined-adornment-password"
                      label="Password"
                      type={showPassword ? 'text' : 'password'}
                      className={classes.textField}
                      value={password}
                      onChange={e => setPassword(e.target.value)}
                      margin="normal"
                      variant="outlined"
                      InputProps={{
                        endAdornment: (
                          <InputAdornment position="end">
                            <IconButton
                              aria-label="Toggle password visibility"
                              onClick={() => setShowPassword(!showPassword)}
                            >
                              {!showPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                          </InputAdornment>
                        ),
                      }}
                    />
                    <Fab
                      variant="extended"
                      size="medium"
                      color="primary"
                      aria-label="Add"
                      className={classes.margin}
                      onClick={() => loginUser({ username, password })}
                    >
                      <NavigationIcon className={classes.extendedIcon} />
                      Login
                    </Fab>
                    <SignupModal signup={signUp} />
                </div>
            </form>
        </div >
    </div>
  )
}

const mapStateToProps = state => {
  return {
    user: state.userReducer.userDetails,
    loggedIn: state.userReducer.loggedIn
  }
}

const mapDispathToProps = dispatch => {
  return {
    loginUser: userLog => dispatch(LoginUser(userLog)),
    signUp: userReg => dispatch(SignUp(userReg)),
    isLogged: () => dispatch(IsLogged())
  }
}

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  dense: {
    marginTop: 16,
  },
  menu: {
    width: 200,
  },
  margin: {
    margin: theme.spacing.unit,
  },
  extendedIcon: {
    marginRight: theme.spacing.unit,
  }
})

export default connect(mapStateToProps, mapDispathToProps)(withStyles(styles)(Login))

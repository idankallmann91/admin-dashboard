import { combineReducers } from 'redux'

import userReducer from './reducers/user-r'
import boardReducer from './reducers/board-r'
import filesReducer from './reducers/files-r'

const rootReducer = combineReducers({
  userReducer,
  boardReducer,
  filesReducer
})

export default rootReducer

export const AddImageFiles = files => {
  return (dispatch, getState) => {
    let size = getState().filesReducer.imageFiles.length + files.length
    if(size >= 25) return
    dispatch({ type: 'ADD_IMAGE_FILES', files })
  }
}

export const AddAudioFile = file => {
  return (dispatch, getState) => {
    dispatch({ type: 'ADD_AUDIO_FILE', file })
  }
}

export const RemoveImageFile = id => {
  return (dispatch, getState) => {
    dispatch({ type: 'REMOVE_IMAGE_FILES', id})
  }
}

export const RemoveAudioFile = id => {
  return (dispatch, getState) => {
    dispatch({ type: 'REMOVE_AUDIO_FILE', id })
  }
}

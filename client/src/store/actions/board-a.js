import { post, get } from 'axios'

export const UploadFiles = files => {
  return (dispatch) => {
    post('http://localhost:4000/api/upload-files', files)
      .then(res => {
        console.log(res)
      })
  }
}

export const ToggleButton = () => {
  return (dispatch, getState) => {
    let keyBtn = getState().boardReducer.keyboard
    get(`http://localhost:4000/api/toggle-button?btn=${keyBtn}`)
      .then(res => {
        console.log(res)
        dispatch({ type: 'ON_OFF', keyboard: res.data.keyboard })
      })
  }
}

export const SetCheckbox = (check) => {
  return (dispatch, getState) => dispatch({type: 'CHECKBOX', check})
}

export const SetText = text => (dispatch) => dispatch({ type: 'INPUT_TEXT', text})

export const SaveText = text => {
  return (dispatch) => {
    get(`http://localhost:4000/api/text-field?text=${text}`)
      .then(res => {
        console.log(res)
      })
  }
}

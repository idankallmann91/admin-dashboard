import { post } from 'axios'

export const LoginUser = userLogObj => {
  return (dispatch) => {
    post('http://localhost:4000/user-login', userLogObj).then(res => {
      let { success, mssg } = res.data
      if(res.data.success) {
        dispatch({ type: 'LOGIN_SUCCESS', success, mssg })
      } else {
        dispatch({ type: 'LOGIN_ERROR', success, mssg })
      }
    }).catch(console.log)
  }
}

export const SignUp = userRegObj => {
  return (dispatch, getState) => {
    post('http://localhost:4000/user-signup', userRegObj)
      .then(res => {
        console.log(res)
        if(res.data.success){
           dispatch({ type: 'SIGNUP_SUCCESS', user: res.data.data })
        } else {
          dispatch({ type: 'SIGNUP_ERROR' })
        }
      })
  }
}

export const SignOut = (credentials) =>
  (dispatch, getState) => dispatch({ type: 'SIGNOUT' })


export const IsLogged = () => {
  return dispatch => {
    return post('http://localhost:4000/me')
      .then(res => dispatch({ type: 'IS_LOGGED', user: res.data }))
  }
}

const db = require('./nedb.js'),
      bcrypt = require('bcrypt')

const findUserByUsername = username => {
  return new Promise((resolve, reject) => {
    db.users.findOne({username: username}, (err, user) => {
      err ? reject(err) : resolve(user)
    })
  })
}

const createUser = user => {
  return new Promise((resolve, reject) => {
    db.users.insert(user, (err) => {
      err ? reject(err) : resolve(true)
    })
  })
}

const deleteUser = id => {
  return new Promise(function(resolve, reject) {
    db.users.remove({_id: id}, (err, numRemoved) => {
      err ? reject(err) : resolve(numRemoved)
    })
  })
}

const cryptPassword = password => {
  let salt = 10
  return new Promise((resolve, reject) => {
    bcrypt.hash(password, salt, (err, hash) => {
      err ? reject(err) : resolve(hash)
    })
  })
}

const comparePassword = (password, hash) => {
  return new Promise((resolve, reject) => {
    bcrypt.compare(password, hash, (err, res) => {
      err ? reject(err) : resolve(res)
    })
  })
}

module.exports = {
  findUserByUsername,
  createUser,
  deleteUser,
  cryptPassword,
  comparePassword
}
